import requests
import os


def test_weather_in_the_city():
    latitude = os.getenv("CITY_LATITUDE", 50.049683)
    longitude = os.getenv("CITY_LONGITUDE", 19.944544)
    weather_url = "https://api.open-meteo.com/v1/forecast"
    params = {"latitude": latitude, "longitude": longitude}
    get_weather_for_city_response = requests.get(weather_url, params=params)
    assert get_weather_for_city_response.status_code == 200
